from setuptools import setup

setup(
    name='TeraBoxer',
    version='1.0.0',
    packages=['teraboxer'],
    url='https://gitlab.com/anon04/TBER',
    license='',
    author='Anonymous',
    author_email='',
    description='python uploader for terabox.com',
    install_requires=['requests', 'tqdm']
)
